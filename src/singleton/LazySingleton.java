package singleton;

public class LazySingleton {

	// 클래스가 초기화 되는 시점에서는 정적 필드를 선언해두고 null로 초기화된다.
	private static LazySingleton Lazy;
	
	// 싱글톤 패턴은 생성자 호출을 통해 외부에서 인스턴스를 생성하는 것을 제한한다.
	private LazySingleton() {}
	
	public static LazySingleton getInstance() {
		
		if(Lazy == null) {
			Lazy = new LazySingleton();
		}
		
		return Lazy;
	}
	
	
}
