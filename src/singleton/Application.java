package singleton;

public class Application {
	
	public static void main(String[] args) {
		
		
		/* 싱글톤
		 * 
		 * Singleton pattern 이란
		 * 애플리케이션이 시작될 때 어떤 클래스가 최초 한번만 메모리를 할당하고 그 메모리에 인스턴스를 만들어서
		 * 하나의 인스턴스를 공유해서 사용하여 메모리 낭비를 방지할 수 있게 한다. (매번 인스턴스 생성 XX 하나만,)
		 * 
		 * 장점 
		 * 1. 첫번째 사용시 인스턴스를 생성해야 해서 속도 차이가 나지만
		 *    두번째 부터는 생성없이 사용할수 있어서 빠르다.
		 * 2. 인스턴스가 절대적으로 한개만 존재하는 것을 보증할 수있다. 
		 * 
		 * 단점
		 * 1. 싱글톤 인스턴스가 너무 많은 일을 하거나 많은 데이터를 공유하면 결합도가 높아짐(유지보수와 테스트에 문제점 발생)
		 * 2. 동시성 문제를 고려해서 설계해야 하기 때문에 난이도가 있다. 
		 * 
		 * 
		 * 싱글톤 구현 방법
		 * 1. 이른 초기화 (Eager Initialization)
		 * 2. 게으른 초기화 (Lazy Initialization)
		 * */
		
		/* 1. 이른 초기화 구현 */
//		EagerSingleton eager = new EagerSingleton(); // 생성자가 접근제한자가 private라 접ㅈ근 불가
		
		EagerSingleton eager1 = EagerSingleton.getInstance();
		EagerSingleton eager2 = EagerSingleton.getInstance();
		
		System.out.println("eager1의 hashcode : " + eager1.hashCode());
		System.out.println("eager2의 hashcode : " + eager2.hashCode());
		
		
		/* 2. 게으른 초기화 구현 */
	
		LazySingleton Lazy1 = LazySingleton.getInstance();
		LazySingleton Lazy2 = LazySingleton.getInstance();
		
		System.out.println("Lazy1의 hashCode : " + Lazy1.hashCode());
		System.out.println("Lazy2의 hashCode : " + Lazy2.hashCode());
		
		
	}

}























